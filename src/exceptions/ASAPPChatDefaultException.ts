/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { ResponseErrorObject } from "@tsed/common";
import { BadRequest } from "@tsed/exceptions";
import { HealthEnum } from "src/models/entities/Health.enum";

export class ASAPPChatBadRequestException
  extends BadRequest
  implements ResponseErrorObject
{
  headers = {};
  errors = [];

  constructor() {
    super("Bad Request");

    // you can also specify errors field for functional errors (like AJV validation).
    this.errors.push({
      message: HealthEnum["BAD REQUEST"],
    });
  }
}

export class ASAPPChatServerErrorException
  extends BadRequest
  implements ResponseErrorObject
{
  headers = {};
  errors = [];

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(errorMessage: any) {
    super("Server Error");

    // you can also specify errors field for functional errors (like AJV validation).
    this.errors.push({
      message:
        HealthEnum["SERVER ERROR"].toString() + " " + errorMessage.toString(),
    });
  }
}
