import {
  BodyParams,
  Controller,
  Get,
  PathParams,
  Post,
  QueryParams,
} from "@tsed/common";
import { Inject } from "@tsed/di";
import { Authenticate } from "@tsed/passport";
import { Groups, Returns } from "@tsed/schema";
import { MessageRequest } from "src/models/http/MessageRequest.model";
import { MessageResponse } from "src/models/http/MessageResponse.model";
import { MessageService } from "src/services/Message.service";

@Controller("/messages")
export class MessageController {
  @Inject()
  protected messageService: MessageService;

  @Post("/")
  @Returns(200, MessageResponse)
  @Authenticate("jwt")
  async sendMessage(
    @BodyParams() @Groups("creation") request: MessageRequest
  ): Promise<{ id: number }> {
    return this.messageService.sendMessage(request);
  }

  @Get("/:id")
  @(Returns(200, Array).Of(MessageResponse))
  @Authenticate("jwt")
  getAllMessages(
    @PathParams("id") recipientUserId: number
  ): Promise<MessageResponse[]> {
    return this.messageService.getAllMessagesFromUser(recipientUserId);
  }

  @Get("/")
  @(Returns(200, Array).Of(MessageResponse))
  @Authenticate("jwt")
  getAllMessagesFromRecipient(
    @QueryParams("id") recipientUserId: number,
    @QueryParams("start") startingMessageID: number,
    @QueryParams("limit") limit: number
  ): Promise<MessageResponse[]> {
    if (limit === undefined) limit = 100;
    return this.messageService.getAllMessagesFromUserRecipient(
      recipientUserId,
      startingMessageID,
      limit
    );
  }
}
