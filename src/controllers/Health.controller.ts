import { Controller, Get } from "@tsed/common";
import { Health } from "src/models/entities/Health";
import { HealthEnum } from "src/models/entities/Health.enum";

@Controller("/check")
export class HealthController {
  @Get("/")
  get(): Health {
    return new Health(HealthEnum.OK);
  }
}
