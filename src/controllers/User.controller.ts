import { BodyParams, Controller, Get, PathParams, Post } from "@tsed/common";
import { Inject } from "@tsed/di";
import { Authenticate } from "@tsed/passport";
import { Groups, Returns } from "@tsed/schema";
import IUser from "src/models/http/entities/User.inteface";
import { CreateUserRequest } from "src/models/http/UserRequest.model";
import { UserResponse } from "src/models/http/UserResponse.model";
import { UserService } from "src/services/User.service";

@Controller("/users")
export class UserController {
  @Inject()
  protected userService: UserService;

  @Post("/")
  @Returns(200, UserResponse)
  async signupUser(
    @BodyParams() @Groups("creation") request: CreateUserRequest
  ): Promise<{ id: number }> {
    return this.userService.createUser(request);
  }

  @Get("/")
  @(Returns(200, Array).Of(UserResponse))
  @Authenticate("jwt")
  async getAllUsers(): Promise<IUser[]> {
    return this.userService.getAllUsers();
  }

  @Get("/:id")
  @(Returns(200, Array).Of(UserResponse))
  @Authenticate("jwt")
  async getUserById(@PathParams("id") userId: number): Promise<IUser> {
    return this.userService.getUserById(userId, true, true);
  }
}
