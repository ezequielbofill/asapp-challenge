import { User } from ".prisma/client";
import {
  BodyParams,
  Controller,
  Get,
  Post,
  ProviderScope,
  Req,
  Scope,
} from "@tsed/common";
import { Authenticate, Authorize, JWT } from "@tsed/passport";
import { Required, Returns } from "@tsed/schema";
import { AuthService } from "src/services/Auth.service";

@Controller("/token")
export class AuthController {
  @Post("/")
  @Authenticate("login")
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  login(@Req() req: Req, @Required() @BodyParams("username") username: string, @Required() @BodyParams("password") password: string) {
    return req.user.token;
  }

}
