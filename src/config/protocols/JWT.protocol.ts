import { Inject, Req } from "@tsed/common";
import { Unauthorized } from "@tsed/exceptions";
import { Arg, OnVerify, Protocol } from "@tsed/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import IUser from "src/models/http/entities/User.inteface";
import { UserService } from "src/services/User.service";

@Protocol({
  name: "jwt",
  useStrategy: Strategy,
  settings: {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: "thisismysupersecretprivatekey1",
    issuer: "localhost",
    audience: "localhost",
  },
})
export class JwtProtocol implements OnVerify {
  @Inject()
  userService: UserService;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async $onVerify(@Req() req: Req, @Arg(0) jwtPayload: any): Promise<IUser> {
    const user = this.userService.getUserById(jwtPayload.sub);

    if (!user) {
      throw new Unauthorized("Wrong token");
    }

    req.user = user;

    return user;
  }
}
