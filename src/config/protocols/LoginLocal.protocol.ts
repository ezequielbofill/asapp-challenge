import { BodyParams, Req } from "@tsed/common";
import { Constant, Inject } from "@tsed/di";
import { Unauthorized } from "@tsed/exceptions";
import { OnVerify, Protocol } from "@tsed/passport";
import { IStrategyOptions, Strategy } from "passport-local";
import { Credentials } from "src/models/entities/Credentials";
import IUser from "src/models/http/entities/User.inteface";
import { AuthService } from "src/services/Auth.service";
import * as jwt from "jsonwebtoken";

import { UserService } from "src/services/User.service";
import { ExpressUser } from "src/models/entities/ExpressUser.model";
import convertIUserToExpressUserConverter from "src/utils/converters/convertIUserToExpressUserConverter";

@Protocol<IStrategyOptions>({
  name: "login",
  useStrategy: Strategy,
  settings: {
    usernameField: "username",
    passwordField: "password",
  },
})
export class LoginLocalProtocol implements OnVerify {
  @Inject()
  private authService: AuthService;

  @Inject()
  private usersService: UserService;

  @Constant("passport.protocols.jwt.settings")
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  jwtSettings: any;

  async $onVerify(
    @Req() _request: Req,
    @BodyParams() credentials: Credentials
  ): Promise<ExpressUser> {
    const { username, password } = credentials;

    let user: IUser | ExpressUser =
      await this.usersService.getUserByCustomPayload({
        where: { username },
        select: { ...this.usersService.defaultSelectedColumns, password: true },
      });

    if (!user) {
      throw new Unauthorized("Wrong credentials");
    }

    user = convertIUserToExpressUserConverter(user, this.createJwt(user));

    if (!this.authService.verifyPassword(password, user)) {
      throw new Unauthorized("Wrong credentials");
    }

    return user;
  }

  createJwt(user: IUser): string {
    const { issuer, audience, secretOrKey, maxAge = 3600 } = this.jwtSettings;
    const now = Date.now();

    return jwt.sign(
      {
        iss: issuer,
        aud: audience,
        sub: user.id,
        exp: now + maxAge * 1000,
        iat: now,
      },
      secretOrKey
    );
  }
}
