import { Room, Prisma } from ".prisma/client";

export default function (
  rooms: Room[]
): Prisma.Enumerable<Prisma.MessageWhereInput> {
  const messageWhereInput: Prisma.Enumerable<Prisma.MessageWhereInput> = [];

  rooms.forEach((room) => {
    messageWhereInput.push({ recipientRoom: room.id });
  });

  return messageWhereInput;
}
