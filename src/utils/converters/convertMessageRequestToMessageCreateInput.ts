import { Prisma } from ".prisma/client";
import { SendMessageRequest } from "src/models/http/SendMessageRequest.model";

export default function ({
  sender,
  recipient,
  content,
}: SendMessageRequest): Prisma.MessageCreateInput {
  return {
    sender,
    recipientRoom: recipient,
    type: content.type,
    content: content.text,
  };
}
