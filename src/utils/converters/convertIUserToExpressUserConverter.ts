import {
    ExpressUser
} from "src/models/entities/ExpressUser.model";
import IUser from "src/models/http/entities/User.inteface";

export default function ({
    id,
    username,
    password
}: IUser, token ? : string): ExpressUser {
    return new ExpressUser({id, email: username, password, token})
}