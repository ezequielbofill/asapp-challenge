import { HealthEnum } from "./Health.enum";

export class Health {
  health: HealthEnum;

  constructor(_health: HealthEnum) {
    this.health = _health;
  }
}
