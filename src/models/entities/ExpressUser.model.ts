import { UserInfo } from "@tsed/passport";

export class ExpressUser extends UserInfo {
  token: string;

  constructor(props: any) {
    super();

    this.id = props.id;
    this.email = props.username;
    this.password = props.password;
    this.token = props.token;
  }
}
