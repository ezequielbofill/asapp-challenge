export enum HealthEnum {
  OK = "OK",
  "NOT FOUND" = "NOT FOUND",
  "BAD REQUEST" = "BAD REQUEST",
  "SERVER ERROR" = "SERVER ERROR",
}
