export class CreateUserRequest {
  username: string;
  password: string;
  id: number;
}
