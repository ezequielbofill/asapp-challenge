export class SendMessageResponse {
  id: number;
  timestamp: Date;
}
