import { MessageContent } from "./entities/MessageContent.model";

export class SendMessageRequest {
  sender: number;
  recipient: number;
  content: MessageContent;
}
