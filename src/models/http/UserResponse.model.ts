import { Room } from ".prisma/client";

export class UserResponse {
  id: number;
  username: string;
  timestamp: string;
  rooms?: Room[];
}
