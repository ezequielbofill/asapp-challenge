import { MessageContent } from "./entities/MessageContent.model";

export class MessageRequest {
  sender: number;
  recipient: number;
  content: MessageContent;
}
