export enum MessageType {
  string = "string",
  image = "image",
  video = "video",
}
