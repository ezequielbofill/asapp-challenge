import { Message, User as PrismaUser } from ".prisma/client";
import { Room } from "@prisma/client";

export default interface IUser extends PrismaUser {
  id: number;
  username: string;
  password: string;
  timestamp: Date;
  rooms?: Room[];
  messages?: Message[];
}
