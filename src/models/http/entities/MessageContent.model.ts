import { MessageType } from "./MessageType.enum";

export class MessageContent {
  type: MessageType;
  text: string;
}
