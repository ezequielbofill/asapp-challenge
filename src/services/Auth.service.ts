import { User } from ".prisma/client";
import { Service } from "@tsed/common";
import bcryptjs from "bcryptjs";
import passport from "passport";
import { Strategy, ExtractJwt, StrategyOptions } from "passport-jwt";

@Service()
export class AuthService {
  private readonly salt = bcryptjs.genSaltSync(15);

  verifyPassword(password: string, user: User): boolean {
    return bcryptjs.compareSync(password, user.password);
  }

  encryptPassword(password: string): string {
    return bcryptjs.hashSync(password, this.salt);
  }

  getToken(): Express.User | undefined {
    const opts: StrategyOptions = {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: "secret",
      issuer: "accounts.examplesoft.com",
      audience: "yoursite.net",
    };

    passport.use(
      new Strategy(opts, function (jwt_payload, done) {
        User.findOne(
          {
            id: jwt_payload.sub,
          },
          function (err, user) {
            if (err) {
              return done(err, false);
            }
            if (user) {
              return done(null, user);
            } else {
              return done(null, false);
              // or you could create a new account
            }
          }
        );
      })
    );
  }
}
