import { Room } from ".prisma/client";
import { Service } from "@tsed/common";
import { Inject } from "@tsed/di";
import { ASAPPChatServerErrorException } from "src/exceptions/ASAPPChatDefaultException";

import { MessageResponse } from "src/models/http/MessageResponse.model";
import { SendMessageRequest } from "src/models/http/SendMessageRequest.model";
import { MessageRepository } from "src/repositories/Message.repository";
import { RoomRepository } from "src/repositories/Room.repository";
import convertMessageRequestToMessageCreateInput from "src/utils/converters/convertMessageRequestToMessageCreateInput";
import convertRoomsArrayToMessageWhereInputEnumerable from "src/utils/converters/convertRoomsArrayToMessageWhereInputEnumerable";

import { UserService } from "./User.service";

@Service()
export class MessageService {
  @Inject()
  protected messageRepository: MessageRepository;

  @Inject()
  protected roomRepository: RoomRepository;

  @Inject()
  protected userService: UserService;

  async getAllMessagesFromUser(userId: number): Promise<MessageResponse[]> {
    const rooms: Room[] = await this.roomRepository.findMany({
      where: {
        users: { some: { userId } },
      },
    });

    if (rooms.length < 1) {
      throw new ASAPPChatServerErrorException(
        "User does not have rooms to send messages yet."
      );
    }

    const messages = await this.messageRepository.findMany({
      where: {
        OR: convertRoomsArrayToMessageWhereInputEnumerable(rooms),
      },
    });

    if (messages.length < 1) {
      throw new ASAPPChatServerErrorException(
        "User does not have sent any messages yet."
      );
    }

    return messages;
  }

  async sendMessage(request: SendMessageRequest): Promise<{ id: number }> {
    const user = await this.userService.getUserByCustomPayload({
      id: request.sender,
      include: {
        rooms: { include: { room: true, user: true } },
      },
    });

    if (user === null) {
      throw new ASAPPChatServerErrorException("Room does not exist");
    }

    let roomId = user.rooms?.find((room) => room.id === request.sender)?.id;

    if (roomId === null || roomId === undefined) {
      roomId = (
        await this.roomRepository.create({
          data: {
            roomType: "private",
            users: {
              create: [
                {
                  userId: request.sender,
                },
                {
                  userId: request.recipient,
                },
              ],
            },
          },
        })
      ).id;
    }

    request.recipient = roomId;

    return this.messageRepository.create({
      data: convertMessageRequestToMessageCreateInput(request),
    });
  }

  async getAllMessagesFromUserRecipient(
    recipientUserId: number,
    startingMessageID: number,
    limit: number
  ): Promise<MessageResponse[]> {
    return this.messageRepository.findMany({
      where: {
        id: { gte: startingMessageID },
        sender: {
          not: recipientUserId,
        },
        Room: {
          users: {
            some: {
              userId: recipientUserId,
            },
          },
        },
      },
      take: limit,
    });
  }
}
