import { Prisma } from "@prisma/client";
import { Service } from "@tsed/common";
import { Inject } from "@tsed/di";
import { ASAPPChatServerErrorException } from "src/exceptions/ASAPPChatDefaultException";
import IUser from "src/models/http/entities/User.inteface";
import { CreateUserRequest } from "src/models/http/UserRequest.model";
import { UserRepository } from "src/repositories/User.repository";
import { AuthService } from "./Auth.service";

type FindUniqueParameters = {
  id?: number;
  where?: Prisma.UserWhereUniqueInput;
  select?: Prisma.UserSelect;
  include?: Prisma.UserInclude;
};

@Service()
export class UserService {
  @Inject()
  private userRepository: UserRepository;

  @Inject()
  private authService: AuthService;

  public readonly defaultSelectedColumns = {
    id: true,
    username: true,
    timestamp: true,
    rooms: true,
    messages: true,
  };

  private readonly defaultIncludedProperties = {
    rooms: { include: { room: true, user: true } },
  };

  async createUser(user: CreateUserRequest): Promise<{ id: number }> {
    try {
      user.password = this.authService.encryptPassword(user.password);

      const createdUser = await this.userRepository.create({ data: user });

      return { id: createdUser.id };
    } catch (e) {
      throw new ASAPPChatServerErrorException(e);
    }
  }

  async getAllUsers(select = this.defaultSelectedColumns): Promise<IUser[]> {
    return this.userRepository.findMany({
      select,
    });
  }

  async getUserById(
    id: number,
    rooms = false,
    messages = false
  ): Promise<IUser> {
    return this.findUserById({
      where: { id },
      select: {
        ...this.defaultSelectedColumns,
        rooms,
        messages,
      },
    });
  }

  async getUserByCustomPayload(args: FindUniqueParameters): Promise<IUser> {
    return this.findUserById(this.getFindUniqueArgs(args));
  }

  private async findUserById(args: Prisma.UserFindUniqueArgs): Promise<IUser> {
    return this.userRepository.findUnique(args).then((user) => {
      if (user === null)
        throw new ASAPPChatServerErrorException("User does not exist");
      return user;
    });
  }

  private getFindUniqueArgs({
    id,
    where,
    include,
    select,
  }: FindUniqueParameters): Prisma.UserFindUniqueArgs {
    if (where === undefined) where = { id };

    if (select !== undefined && include !== undefined) {
      throw Error("eso no se puede po");
    } else if (select !== undefined && include === undefined) {
      return {
        where,
        select,
      };
    } else if (select === undefined && include !== undefined) {
      return {
        where,
        include,
      };
    } else {
      return {
        where,
        select: this.defaultSelectedColumns,
      };
    }
  }
}
