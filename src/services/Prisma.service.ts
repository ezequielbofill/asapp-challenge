import { PrismaClient } from "@prisma/client";
import { Service, OnDestroy, OnInit } from "@tsed/common";

@Service()
export class PrismaService extends PrismaClient implements OnInit, OnDestroy {
  async $onInit(): Promise<void> {
    await this.$connect();
  }
  async $onDestroy(): Promise<void> {
    await this.$disconnect();
  }
}
