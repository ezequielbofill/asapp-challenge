import { Inject, Injectable } from "@tsed/di";
import { Prisma } from "@prisma/client";
import { PrismaService } from "../services/Prisma.service";
import IUser from "src/models/http/entities/User.inteface";

@Injectable()
export class UserRepository {
  @Inject()
  prisma: PrismaService;

  async findUnique(args: Prisma.UserFindUniqueArgs): Promise<IUser | null> {
    return this.prisma.user.findUnique(args);
  }

  async findMany(args?: Prisma.UserFindManyArgs): Promise<IUser[]> {
    return this.prisma.user.findMany(args);
  }

  async create(args: Prisma.UserCreateArgs): Promise<IUser> {
    return this.prisma.user.create(args);
  }

  async update(args: Prisma.UserUpdateArgs): Promise<IUser> {
    return this.prisma.user.update(args);
  }

  async delete(args: Prisma.UserDeleteArgs): Promise<IUser> {
    return this.prisma.user.delete(args);
  }
}
