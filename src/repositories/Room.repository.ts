import { Inject, Injectable } from "@tsed/di";
import { Prisma, Room } from "@prisma/client";
import { PrismaService } from "../services/Prisma.service";

@Injectable()
export class RoomRepository {
  @Inject()
  prisma: PrismaService;

  async findUnique(args: Prisma.RoomFindUniqueArgs): Promise<Room | null> {
    return this.prisma.room.findUnique(args);
  }

  async findMany(args?: Prisma.RoomFindManyArgs): Promise<Room[]> {
    return this.prisma.room.findMany(args);
  }

  async create(args: Prisma.RoomCreateArgs): Promise<Room> {
    return this.prisma.room.create(args);
  }

  async update(args: Prisma.RoomUpdateArgs): Promise<Room> {
    return this.prisma.room.update(args);
  }

  async delete(args: Prisma.RoomDeleteArgs): Promise<Room> {
    return this.prisma.room.delete(args);
  }
}
