import { Inject, Injectable } from "@tsed/di";
import { Prisma, Message } from "@prisma/client";
import { PrismaService } from "../services/Prisma.service";

@Injectable()
export class MessageRepository {
  @Inject()
  prisma: PrismaService;

  async findUnique(
    args: Prisma.MessageFindUniqueArgs
  ): Promise<Message | null> {
    return this.prisma.message.findUnique(args);
  }

  async findMany(args?: Prisma.MessageFindManyArgs): Promise<Message[]> {
    return this.prisma.message.findMany(args);
  }

  async create(args: Prisma.MessageCreateArgs): Promise<Message> {
    return this.prisma.message.create(args);
  }

  async update(args: Prisma.MessageUpdateArgs): Promise<Message> {
    return this.prisma.message.update(args);
  }

  async delete(args: Prisma.MessageDeleteArgs): Promise<Message> {
    return this.prisma.message.delete(args);
  }
}
