# ASAPP Chat Backend Challenge v1

### Prerequisites

Installed Nodejs >= v8.x

## Build setup

> **Important!** Ts.ED requires Node >= 10, Express >= 4 and TypeScript >= 3.

```batch
# install dependencies
$ npm install

# initiate prisma ORM
$ npm run prisma:migrate
```

### How to run it

```batch
# serve
$ npm run start
```

The server will start at http://localhost:8080.